﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterEventTextBox
{
    public class Parcel
    {
        public Parcel(string parcelNumber)
        {
            ParcelNumber = parcelNumber;
        }

        private string parcelNumber;
        public string ParcelNumber
        {
            get { return parcelNumber; }
            set { parcelNumber = value; }
        }

    }
}
