﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace EnterEventTextBox
{
    public class Shipper
    {
        public string ShipperName { get; set; }

        private bool isUnrolled;
        public bool IsUnrolled
        {
            get { return isUnrolled; }
            set
            {
                isUnrolled = value;
            }
        }

        private int parcelAmount;
        public int ParcelAmount
        {
            get
            {
                return parcelAmount;
            }
            set
            {
                parcelAmount = value;
            }
        }

        ObservableCollection<Parcel> parcels = new ObservableCollection<Parcel>();
        public ObservableCollection<Parcel> Parcels
        {
            get
            {
                if (!IsUnrolled)
                {
                    InvisibleParcels.AddRange(parcels);
                    parcels.Clear();
                    return parcels;
                }
                else
                {
                    foreach (var item in InvisibleParcels)
                    {
                        parcels.Add(item);
                    }
                    InvisibleParcels.Clear();
                    return parcels;
                }
            }
        }

        private List<Parcel> invisibleParcels = new List<Parcel>();
        public List<Parcel> InvisibleParcels
        {
            get { return invisibleParcels; }
            set { invisibleParcels = value; }
        }

        public void InitializeParcels()
        {
            for (int i = 1; i <= ParcelAmount; i++)
            {
                parcels.Add(new Parcel(i.ToString()));
            }
        }

        public string BtnLabelShipper
        {
            get { return String.Format("{0} {1} ({2})", ShipperName, Environment.NewLine, ParcelAmount); }
        }
    }
}
