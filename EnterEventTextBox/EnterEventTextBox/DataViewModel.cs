﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterEventTextBox
{
    public class DataViewModel : Screen
    {
        public DataViewModel()
        {
            // button factory
            Shipper shp;
            for (int i = 0; i < 3; i++)
            {
                shp = new Shipper() { ParcelAmount = 5, ShipperName = "DPDdpd", IsUnrolled = false };
                shp.InitializeParcels();
                Shippers.Add(shp);
                shp = new Shipper() { ParcelAmount = 1, ShipperName = "GEISgeis", IsUnrolled = false };
                shp.InitializeParcels();
                Shippers.Add(shp);
            }
            shp = new Shipper() { ParcelAmount = 100, ShipperName = "PPLppl", IsUnrolled = false };
            shp.InitializeParcels();
            Shippers.Add(shp);
        }

        public void ShipperIsClicked(Shipper spOld, bool isChecked)
        {
            spOld.IsUnrolled = isChecked;
            RefreshView();
        }

        private ObservableCollection<Shipper> shippers = new ObservableCollection<Shipper>();
        public ObservableCollection<Shipper> Shippers
        {
            get { return shippers; }
            set
            {
                shippers = value;
                NotifyOfPropertyChange(() => Shippers);
            }
        }

        public void RemoveShipper()//EraseParcel() přejmenovat
        {
            Shipper shipper = Shippers.ElementAt(0);
            if (shipper.ParcelAmount <= 1)
            {
                Shippers.Remove(shipper);
            }
            else
            {
                if (shipper.Parcels.Count > 0)
                {
                    shipper.ParcelAmount--;
                    shipper.Parcels.RemoveAt(0);
                }
                else if (shipper.InvisibleParcels.Count > 0)
                {
                    shipper.ParcelAmount--;
                    shipper.InvisibleParcels.RemoveAt(0);
                }
                RefreshView();
            }
        }

        public void RefreshView()
        {
            List<Shipper> temporaryList = new List<Shipper>();
            temporaryList.AddRange(Shippers);
            Shippers.Clear();
            foreach (var item in temporaryList)
            {
                Shippers.Add(item);
            }
            temporaryList.Clear();
        }
    }
}
